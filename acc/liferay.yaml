apiVersion: v1
kind: Template
metadata:
  name: "liferay-71-dxp-test"
  labels:
    app: liferay-71-dxp-test
    template: liferay-71-dxp-test
  annotations:
    openshift.io/display-name: Liferay 7.1 DXP TEST
    iconClass: proteon-icon-liferay-firelay
    template.openshift.io/bindable: "true"
    description: >-
      Liferay 7.1 DXP
    openshift.io/long-description: >-
      Environments created using this template will have Liferay 7.1 DXP. This template is well suited for Acceptance and Production environments.
    tags: "liferay, 7.1, dxp, S2I, proteon, firelay, tomcat, java"
    openshift.io/provider-display-name: "Proteon, Inc."
    openshift.io/documentation-url: "https://git.firelay.com/openshift/templates/README.md"
    openshift.io/support-url: "https://www.proteon.com/contact/"
objects:
  ### Liferay Deployment ###
  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      annotations:
        template.alpha.openshift.io/wait-for-ready: "true"
      name: ${DEPLOYMENT_NAME}-liferay
    spec:
      replicas: 1
      selector:
        tier: portal
        version: ${LIFERAY_IMAGE_TAG}
        app: ${DEPLOYMENT_NAME}
        env: acc
      strategy:
        type: Recreate
      template:
        metadata:
          labels:
            tier: portal
            version: ${LIFERAY_IMAGE_TAG}
            app: ${DEPLOYMENT_NAME}
            env: acc
        spec:
          serviceAccount: firelay
          serviceAccountName: firelay
          containers:
            - name: liferay
              image: ${DEPLOYMENT_NAME}-liferay:latest
              imagePullPolicy: ${IMAGE_PULL_POLICY}
              env:
                - name: DATABASE_HOST
                  value: ${DB_HOST}
                - name: DATABASE_NAME
                  value: ${DB_NAME}
                - name: DATABASE_USER
                  valueFrom:
                    secretKeyRef:
                      name: ${DB_SECRET_NAME}
                      key: DB_USER
                - name: DATABASE_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      name: ${DB_SECRET_NAME}
                      key: DB_PASSWORD
                - name: DATABASE_TYPE
                  value: ${DB_TYPE}
                - name: LIFERAY_SET_WIZARD_PROPERTIES
                  value: "true"
                - name: LIFERAY_CLUSTER_ENABLE
                  value: ${LIFERAY_CLUSTER_ENABLE}
                - name: LIFERAY_FROM_ADDRESS
                  value: ${LIFERAY_FROM_ADDRESS}
                - name: LIFERAY_FROM_NAME
                  value: ${LIFERAY_FROM_NAME}
                - name: LIFERAY_DEFAULT_WEB_ID
                  value: ${LIFERAY_DEFAULT_WEB_ID}
                - name: LIFERAY_EMAIL_PREFIX
                  value: ${LIFERAY_EMAIL_PREFIX}
                - name: LIFERAY_SAMPLE_DATA
                  value: ${LIFERAY_SAMPLE_DATA}
                - name: LIFERAY_WIZARD
                  value: "false"
                - name: LIFERAY_MODULE_MODE
                  value: ${LIFERAY_MODULE_MODE}
                - name: DEBUG_SLEEP
                  value: ${DEBUG_SLEEP}
                - name: CLEAN
                  value: "false"
              ports:
                - containerPort: 8080
                  name: web
                  protocol: TCP
              lifecycle:
                preStop:
                  exec:
                    command: ["/liferay/home/tomcat/bin/shutdown.sh"]
              readinessProbe:
                # so we can check every 10 sec for 15+ min
                failureThreshold: 100
                httpGet:
                  path: /o/js_bundle_config
                  port: 8080
                  scheme: HTTP
                initialDelaySeconds: 120
                periodSeconds: 10
                successThreshold: 1
                timeoutSeconds: 300
              livenessProbe:
                httpGet:
                  path: /o/js_bundle_config
                  port: 8080
                initialDelaySeconds: 600
                periodSeconds: 60
                timeoutSeconds: 30
                failureThreshold: 3
              resources:
                limits:
                  cpu: "${LIFERAY_CPU_LIMIT}"
                  memory: ${LIFERAY_MEMORY_LIMIT}
                requests:
                  cpu: "${LIFERAY_CPU_REQUEST}"
                  memory: ${LIFERAY_MEMORY_REQUEST}
              volumeMounts:
                - name: no-api-access-please
                  mountPath: /var/run/secrets/kubernetes.io/serviceaccount
                - name: configmap
                  mountPath: /liferay/config
                - name: temp
                  mountPath: /liferay/temp
                - name: data
                  mountPath: /liferay/data
                - name: deployment
                  mountPath: /liferay/home/deployment
          volumes:
            - name: no-api-access-please
              emptyDir: {}
            - name: temp
              emptyDir: {}
            - name: deployment
              emptyDir: {}
            - name: configmap
              configMap:
                defaultMode: 440
                name: ${DEPLOYMENT_NAME}-liferay
            - name: data
              persistentVolumeClaim:
                claimName: ${DEPLOYMENT_NAME}-liferay
      triggers:
        - type: ConfigChange
        - type: ImageChange
          imageChangeParams:
            automatic: true
            containerNames:
              - liferay
            from:
              kind: ImageStreamTag
              name: "${DEPLOYMENT_NAME}-liferay:latest"

  ### Liferay ConfigMap ###
  - apiVersion: v1
    kind: ConfigMap
    metadata:
      labels:
        tier: portal
        version: ${LIFERAY_IMAGE_TAG}
        app: ${DEPLOYMENT_NAME}
        env: acc
      name: ${DEPLOYMENT_NAME}-liferay
    data:
      ROOT.xml: |+
        <Context crossContext="true" path="">
        </Context>
      bundle-blacklist.cfg: |+
        blacklistBundleSymbolicNames=["com.liferay.portal.search.elasticsearch","com.liferay.portal.search.elasticsearch.shield","com.liferay.portal.search.elasticsearch.marvel.web"]
      portal-ext.properties: |+
        cluster.link.enabled=${LIFERAY_CLUSTER_ENABLE}
        cluster.link.autodetect.address=DB.Server.IP:Port
        ehcache.cluster.link.replication.enabled=${LIFERAY_CLUSTER_ENABLE}
        web.server.display.node=false
        web.server.protocol=https
        web.server.https.port=443
        company.security.auth.requires.https=true
        redirect.url.security.mode=ip
        redirect.url.domains.allowed=
        redirect.url.ips.allowed=
        users.reminder.queries.enabled=false
        users.reminder.queries.custom.question.enabled=false
        mail.session.mail.smtp.auth=false
        mail.session.mail.smtp.host=${SMTP_HOST}
        mail.session.mail.smtp.port=${SMTP_PORT}
        setup.wizard.add.sample.data=off
        setup.wizard.enabled=false
        admin.email.from.address=test@liferay.com
        admin.email.from.name=Joe Bloggs
        default.admin.email.address.prefix=test
        terms.of.use.required=false
        dl.store.antivirus.enabled=${LIFERAY_ENABLE_ANTIVIRUS} 
        dl.store.antivirus.impl=com.liferay.portlet.documentlibrary.antivirus.ClamAntivirusScannerImpl
        module.framework.properties.osgi.console=0.0.0.0:11311
        module.framework.properties.lpkg.index.validator.enabled=false
        com.liferay.portal.servlet.filters.sso.cas.CASFilter=false
        com.liferay.portal.servlet.filters.sso.ntlm.NtlmFilter=false
        com.liferay.portal.servlet.filters.sso.ntlm.NtlmPostFilter=false
        com.liferay.portal.servlet.filters.sso.opensso.OpenSSOFilter=false
        com.liferay.portal.sharepoint.SharepointFilter=false
        com.liferay.portal.servlet.filters.gzip.GZipFilter=false
        locales=en_US,es_ES,fr_FR,nl_NL
        locales.enabled=en_US,es_ES,fr_FR,nl_NL
        company.default.locale=${LIFERAY_DEFAULT_LOCALE}
        company.default.name=${LIFERAY_COMPANY_NAME}
        layout.show.portlet.access.denied=false
        company.default.web.id=liferay
        terms.of.use.required=false
        users.reminder.queries.enabled=false
        session.timeout=60
        session.timeout.warning=2
      server.xml: |+
        <?xml version='1.0' encoding='utf-8'?>
        <Server port="8005" shutdown="SHUTDOWN">
          <Listener className="org.apache.catalina.startup.VersionLoggerListener" />
          <Listener className="org.apache.catalina.core.AprLifecycleListener" SSLEngine="on" />
          <Listener className="org.apache.catalina.core.JreMemoryLeakPreventionListener" />
          <Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener" />
          <Listener className="org.apache.catalina.core.ThreadLocalLeakPreventionListener" />
          <GlobalNamingResources>
            <Resource name="UserDatabase" auth="Container"
              type="org.apache.catalina.UserDatabase"
              description="User database that can be updated and saved"
              factory="org.apache.catalina.users.MemoryUserDatabaseFactory"
              pathname="conf/tomcat-users.xml" />
          </GlobalNamingResources>
          <Service name="Catalina">
            <Connector port="8080" protocol="HTTP/1.1"
              proxyPort="443" scheme="https" 
              secure="true" connectionTimeout="20000"
              redirectPort="8443" URIEncoding="UTF-8" />
            <Connector port="8009" protocol="AJP/1.3" redirectPort="8443" URIEncoding="UTF-8" />
            <Engine name="Catalina" defaultHost="localhost">
              <Realm className="org.apache.catalina.realm.LockOutRealm">
                <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
                resourceName="UserDatabase"/>
              </Realm>
              <Host name="localhost"  appBase="webapps"
                unpackWARs="true" autoDeploy="true">
                <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
                  prefix="localhost_access_log" suffix=".txt"
                  pattern="%h %l %u %t &quot;%r&quot; %s %b" />
              </Host>
              <Cluster className="org.apache.catalina.ha.tcp.SimpleTcpCluster" channelSendOptions="8">
                <Manager className="org.apache.catalina.ha.session.DeltaManager" expireSessionsOnShutdown="false" notifyListenersOnReplication="true"></Manager>
                <Channel className="org.apache.catalina.tribes.group.GroupChannel">
                  <Membership className="org.apache.catalina.tribes.membership.McastService" address="224.0.0.5" port="45564" frequency="500" dropTime="3000"></Membership>
                  <Sender className="org.apache.catalina.tribes.transport.ReplicationTransmitter">
                    <Transport className="org.apache.catalina.tribes.transport.nio.PooledParallelSender"></Transport>
                  </Sender>
                  <Receiver className="org.apache.catalina.tribes.transport.nio.NioReceiver"
                  address="auto" port="4000" autoBind="100" selectorTimeout="5000"  maxThreads="6">
                  </Receiver>
                  <Interceptor className="org.apache.catalina.tribes.group.interceptors.TcpFailureDetector">
                  </Interceptor>
                  <Interceptor className="org.apache.catalina.tribes.group.interceptors.MessageDispatchInterceptor">
                  </Interceptor>
                </Channel>
                <Valve className="org.apache.catalina.ha.tcp.ReplicationValve" filter=""></Valve>
                <Valve className="org.apache.catalina.ha.session.JvmRouteBinderValve"></Valve>
                <ClusterListener className="org.apache.catalina.ha.session.ClusterSessionListener">
                </ClusterListener>
              </Cluster>
            </Engine>
          </Service>
        </Server>
      elasticsearch.cfg: |+
        operationMode="REMOTE"
        clientTransportIgnoreClusterName="true"
        retryOnConflict="5"
        logExceptionsOnly="false"
        httpEnabled="true"
        transportAddresses="${DEPLOYMENT_NAME}-es:9300"
        discoveryZenPingUnicastHostsPort="9300-9400"
        clusterName="${ES_CLUSTER_NAME}"
      setenv.sh: |
        CATALINA_OPTS="$CATALINA_OPTS -Dfile.encoding=UTF8
        -Djava.net.preferIPv4Stack=true
        -Dorg.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES=false
        -Duser.timezone=GMT -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"
      portlet-ext.properties: |
        bpost.webservice.url=https://webservices-ctrl-ma.acbpost.be/ws/AtHomeInternalCS_v2_1
      com.liferay.portal.search.configuration.IndexStatusManagerConfiguration.config: |
        indexReadOnly="false"
      com.liferay.portal.store.file.system.configuration.AdvancedFileSystemStoreConfiguration.config: |
      license.xml: |+
        ${LICENSE}

  ### Liferay Service ###
  - apiVersion: v1
    kind: Service
    metadata:
      labels:
        tier: portal
        version: ${LIFERAY_IMAGE_TAG}
        app: ${DEPLOYMENT_NAME}
        env: acc
      name: ${DEPLOYMENT_NAME}-liferay
    spec:
      selector:
        tier: portal
        version: ${LIFERAY_IMAGE_TAG}
        app: ${DEPLOYMENT_NAME}
        env: acc
      ports:
        - name: web
          port: 80
          protocol: TCP
          targetPort: 8080
        - name: shell
          port: 11311
          protocol: TCP
          targetPort: 11311
      type: NodePort

  ### Liferay Route ###
  - apiVersion: v1
    kind: Route
    metadata:
      labels:
        tier: portal
        version: ${LIFERAY_IMAGE_TAG}
        app: ${DEPLOYMENT_NAME}
        env: acc
      name: ${DEPLOYMENT_NAME}-liferay
      annotations:
        kubernetes.io/tls-acme: "${ENABLE_SSL}"
    spec:
      port:
        targetPort: web
      tls:
        termination: edge
      to:
        kind: Service
        name: ${DEPLOYMENT_NAME}-liferay
      wildcardPolicy: None

  ### Liferay PVC ###
  - apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      labels:
        tier: portal
        version: ${LIFERAY_IMAGE_TAG}
        app: ${DEPLOYMENT_NAME}
        env: acc
      name: ${DEPLOYMENT_NAME}-liferay
    spec:
      accessModes:
        - ReadWriteMany
      resources:
        requests:
          storage: ${LIFERAY_STORAGE_SIZE}Gi


parameters:
  - name: DEPLOYMENT_NAME
    description: Name of deployment
    required: true
    value: bpost
  ### - S2I that builds this image - ###
  - name: LIFERAY_IMAGE
    description: Docker Image for liferay
    required: true
    value: registry.firelay.com/container-engine-images/liferay-portal-enterprise
  - name: LIFERAY_IMAGE_TAG
    description: Docker Image Tag
    value: S2I-dxp-7110-sp1-v1
  ### - S2I that builds this image - ###
  - name: IMAGE_PULL_POLICY
    description: Image Pull Policy for pod
    required: true
    value: IfNotPresent
  - name: LICENSE
    description: Valid liferay dxp license
    required: true
    value: "123"
  - name: LIFERAY_COMPANY_NAME
    description: Liferay company name
    value: Liferay
  - name: LIFERAY_CLUSTER_ENABLE
    description: Enable Cluster Option
    required: true
    value: "false"
  #### DATABASE VARS ####
  - name: DB_NAME
    description: Database name for liferay
    required: true
    value: liferay
  - name: DB_TYPE
    description: Database type
    required: true
    value: mysql
  - name: DB_HOST
    description: Host where Database is located. Should be retrieve from DB Service.
    required: true
    value: bpost-accept-mysql
  - name: DB_SECRET_NAME
    description: Name of the secret for DB credentials
    required: true
    value: bpost-accept-mysql
  #### DATABASE VARS END ####
  - name: ES_CLUSTER_NAME
    description: Elasticsearch cluster name
    required: true
    value: LiferayES
  - name: ENABLE_SSL
    description: Enable Secure Route
    value: "true"
  - name: LIFERAY_STORAGE_SIZE
    description: Storage size to be used in Gi
    required: true
    value: "50"
  - name: LIFERAY_FROM_ADDRESS
    description: Notification email from address
    value: test@liferay.com
  - name: LIFERAY_FROM_NAME
    description: Notification email from name
    value: Joe Bloggs
  - name: LIFERAY_DEFAULT_LOCALE
    description: Default locale of the portal
    value: en_US
  - name: LIFERAY_DEFAULT_WEB_ID
    description: Default web ID
    value: liferay.com
  - name: LIFERAY_EMAIL_PREFIX
    description: Default admin email address prefix
    value: test
  - name: LIFERAY_SAMPLE_DATA
    description: To add sample data on the first startup
    value: "off"
  - name: LIFERAY_ENABLE_ANTIVIRUS
    description: Enable or disable antivirus
    value: "false"
  - name: SMTP_HOST
    description: SMTP server used to send liferay emails
    required: true
    value: smtp.default.svc
  - name: SMTP_PORT
    description: SMTP port used to send liferay emails
    required: true
    value: "2525"
  - name: LIFERAY_SERVICE_ACCOUNT
    description: Service Account used to deploy liferay instance
    required: true
    value: firelay
  - name: LIFERAY_MEMORY_REQUEST
    description: Requested memory for liferay instance *Remeber to add Mi or Gi
    value: "6Gi"
  - name: LIFERAY_MEMORY_LIMIT
    description: Max Memory limit for liferay instance *Remeber to add Mi or Gi
    value: "8Gi"
  - name: LIFERAY_CPU_REQUEST
    description: Requested CPU for liferay instance in Core units
    value: "2"
  - name: LIFERAY_CPU_LIMIT
    description: Max CPU limit for liferay instance in Core units
    value: "4"
  - name: LIFERAY_MODULE_MODE
    description: Set the origin for liferay modules Supported values ON_IMAGE | ON_PVC
    value: ON_IMAGE
  - name: DEBUG_SLEEP
    description: To sleep before starting Liferay
    value: "0"
  - name: DEPLOYMENT_TAG
    description: Tag to deploy
    required: true
    value: 1.0.0  